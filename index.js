require("dotenv").config();
const express = require("express");
const cors = require("cors");

//APP CONFIG
const app = express();
const port = process.env.PORT;

//MIDDLEWARES
app.use(express.json());
app.use(cors());

//ROUTES
app.get("/", function (req, res) {
  res.status(200).json({
    success: true,
    data: "Server Setup Completed Successfully!!",
    error: null,
  });
});

//LISTEN
app.listen(port, function () {
  console.log(`Server running on port:${port}`);
});
